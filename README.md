# Theia Web Video Server Implementation

# How to Implement Camera View on Theia Editors
* Add this repo to configuration.yml:
``` yaml
requirements:
  packages:
    - 'https://gitlab.com/riders-content-team/theia-ros-web-video-server@@main'
```

* Add this line at the top of the autorun.sh to remove previous camera files:
``` bash
rm -rf /workspace/src/.riders/commands/5-Camera
``` 

* Add this lines to end of your start.launch file:
```xml
  <node name="run_web_video_server" pkg="theia-ros-web-video-server" type="Camera_Run.sh" />
  <node name="update_camera_topics" pkg="theia-ros-web-video-server" type="Camera_Update.sh" />
```

* Add this line to the end of your reset.launch file:
```xml
  <node name="update_camera_topics" pkg="theia-ros-web-video-server" type="Camera_Update.sh" />
```

* Kill web_video_server on Stop.sh
```bash
rosnode kill web_video_server
```

* Also don't forget to add this line to end of your Stop.sh file to remove unwanted files.
```bash
source /workspace/src/_lib/theia-ros-web-video-server/Camera_Reset.sh
```
