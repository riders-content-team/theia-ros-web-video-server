#!/bin/bash

source ${RIDERS_GLOBAL_PATH}/lib/helpers.sh
export CAMERA_WEB_URI="$(ride_ports_get_address 8060)"
mkdir /workspace/src/.riders/commands/5-Cameras

image_topics="$(rostopic find sensor_msgs/Image)"

commands_file="/workspace/src/.riders/commands/5-Cameras/commands.yml"
echo "commands:" > $commands_file

for topic in $image_topics
do
    export CAMERA_WEB_URI="$(ride_ports_get_address 8060)/stream?topic=$topic"
    topicfile=${topic////.}
    filename="/workspace/src/.riders/commands/5-Cameras/$topicfile.sh"
    html_file="/workspace/src/.riders/commands/5-Cameras/$topicfile.html"

    touch $html_file
    touch $filename
    chmod +x $filename
    
    echo "<!DOCTYPE html>" > $html_file
    echo "<html lang='en'>" >> $html_file
    echo '<body style="background: #000000;">' >> $html_file
    echo "<img style='border:1px solid white; width:80%; max-width:512px; max-height:512px; text-align: center; display: block;-webkit-user-select: none;margin: auto;background-color: hsl(0, 0%, 25%);' src='$CAMERA_WEB_URI'>" >> $html_file
    echo '</body>' >> $html_file
    echo '</html>' >> $html_file

    echo "#!/bin/bash" > $filename
    echo "source $RIDERS_GLOBAL_PATH/lib/helpers.sh" >> $filename
    # hack name
    echo "ride_display_show_iframe 'file://${RIDE_SOURCE_PATH}/.riders/commands/5-Cameras/$topicfile.html' 'Belt Camera'" >> $filename

    echo "  $topicfile.sh:" >> $commands_file
    echo "      displayName: Belt Camera" >> $commands_file
    echo "      showTerminal: false" >> $commands_file
done

#ride_display_show_iframe "$CAMERA_WEB_URI" "Camera"
